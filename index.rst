:hide-toc:
:hide-navigation:

IGWN Documentation Index
========================

Welcome to the International Gravitational-Wave Observatory Network (IGWN)
documentation index! Here you can find documentation for a number of projects
of the LIGO Scientific Collaboration, the Virgo Collaboration, and the KAGRA
Collaboration.

Manuals
-------

* `LIGO/Virgo/KAGRA Public Alerts User Guide </userguide>`_

Projects
--------

* `gwcelery </gwcelery>`_: LIGO/Virgo public alert pipeline
* `igwn-auth-utils </igwn-auth-utils>`_: Python library functions to simplify using IGWN authorisation credentials
